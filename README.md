# Usage

## prerequisites

### Install required libraries

`poetry install` or `pip install -r requirements.txt`

### Get task id and file id.

From the task on Flywheel, you can click inspect and the network tab. The first request has the task_id in it.  Later requests have file id.

## Running

From an ipython window use the following code to get started

```python
# Source functions from converter.py
%run rtstruct_to_fw/converter.py 

# Set task_id and file_id variables
task_id = 'your task id'
file_id = 'your file id'

# Instantiate client
from fw_client import FWClient
fw = FWClient(api_key=<your api key>)

# Load dicoms
from fw_file.dicom import DICOM, DICOMCollection
rt_dcm = DICOM('./resources/rt-struct.dcm')
opt_dcm = DICOM('./resources/opt-dcm.dcm')
ct_dcm = DICOMCollection.from_zip('./resources/ct-dcm.dicom.zip')

# Extract ROIs from dicoms using get_rois function from converter.py
rois, dcm_info = get_rois(rt_dcm, ct_dcm)

# Upload all ROIs as annotations
rois = {r.name: r for r in rois.values()}
for roi in rois.values():
    print(roi.name)
    create_annotation(fw, task_id, file_id, roi, opt_dcm, dcm_info)
```


Or run the following to only get the annotations

```python
%run rtstruct_to_fw/converter.py 

# Set task_id and file_id variables
file_id = 'your file id'

# Instantiate client
from fw_client import FWClient
fw = FWClient(api_key=<your api key>)

# Load dicoms
from fw_file.dicom import DICOM, DICOMCollection
rt_dcm = DICOM('./resources/rt-struct.dcm')
opt_dcm = DICOM('./resources/opt-dcm.dcm')
ct_dcm = DICOMCollection.from_zip('./resources/ct-dcm.dicom.zip')

# Extract ROIs from dicoms using get_rois function from converter.py
rois, dcm_info = get_rois(rt_dcm, ct_dcm)

# Upload all ROIs as annotations
rois = {r.name: r for r in rois.values()}
annotations = []
for roi in rois.values():
    print(roi.name)
    annotations.append(roi_to_annotations(roi, dcm_info))
```
