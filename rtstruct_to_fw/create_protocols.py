import os
import json
from fw_client import FWClient

API_KEY = os.getenv('TASKS_API_KEY')
PROJECT_ID = '62f56bfe6a458711cfcad93d'


def create_form(client):
    with open('./form.json', 'r') as fp:
        data = json.load(fp)
        res = client.post('/api/forms', json=data)
        return res._id


def create_viewer_config(client):
    with open('./viewer_config.json', 'r') as fp:
        data = json.load(fp)
        res = client.post('/api/viewerconfigs', json=data)
        return res._id


def create_reader_protocol(
    client,
    name='default_protocol',
    description='',
):
    form_id = create_form(client)
    viewer_config_id = create_viewer_config(client)
    data = {
        'label': name,
        'description': description,
        'form_id': form_id,
        'viewer_config_id': viewer_config_id,
        'parent': {
            'type': 'project',
            'id': PROJECT_ID,
        }
    }
    res = client.post('/api/read_task_protocols', json=data)
    print(f'Successfully created reader protocol {res.label}')


if __name__ == '__main__':
    client = FWClient(
        api_key=API_KEY,
        client_name='read_task_protocols',
        client_version='0.1.0'
    )
    create_reader_protocol(
        client,
        name='my_protocol',
        description='created by nate'
    )





