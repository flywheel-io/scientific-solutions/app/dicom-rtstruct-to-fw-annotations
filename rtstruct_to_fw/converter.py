import numpy as np
from fw_file.dicom import DICOMCollection, DICOM
from dataclasses import dataclass, field
import typing as t
from shapely.geometry import Polygon
from shapely.ops import unary_union
from centerline.geometry import Centerline
from scipy import interpolate
import os
import uuid

import warnings
warnings.filterwarnings('ignore')

from sklearn.cluster import DBSCAN

from fw_client import FWClient


# For now, hardcode ROI types that need to be deconstructed from multiple
#   disconnected closed polygons to a line.
LAYER_ROIS = [
    'BM',
    'EZ',
    'ELM',
    'OPL-HFL',
    'ILM',
    'IB-RPE'
]


@dataclass
class Contour:
    # Points array
    points: np.ndarray
    # Which frame to draw contour on.
    frame_index: int
    # ROI tool type
    type_: str
    # Bounding box
    left: float
    top: float
    width: float
    height: float


@dataclass
class ROI:
    # RGB value of roi color
    color: list
    # Roi name
    name: str
    # Description
    description: str
    # ROI number
    num: int
    # List of contours from ROI.
    contours: t.List[Contour] = field(default_factory=list)


@dataclass
class DCMInfo:
    # Source DICOM # of rows
    rows: int
    # Source DICOM # of columns
    columns: int
    # Source dicom pixel_spacing to map from real world to pixel space.
    pixel_spacing: tuple
    # Number of slices
    slices: int


def sop_to_frame_mapping(dcmcoll: DICOMCollection):
    """Generate mapping dictionary from reference SOPInstanceUID to frame index."""
    sops = dict()
    for dcm in dcmcoll:
        sops[dcm.SOPInstanceUID] = dcm.InstanceNumber
    return sops


def get_point(p1, p2=None):
    """Helper to get point."""
    point = {
        'x': p1[0],
        'y': p1[1],
        'highlight': True,
        'active': True
    }

    if p2 is not None:
        point['lines'] = [{
            'x': p2[0],
            'y': p2[1]
        }]
    else:
        point['lines'] = []
    return point


def polys_to_centerline(
    contours: t.List[Contour],
    dcm_info: DCMInfo,
    interpolation_distance=2,
    max_gap_distance=10,  # The maximum distance between two samples for one to be considered in same contour
) -> t.Tuple[t.List[dict], t.Tuple[float, float, float, float]]:
    """Collapse a group of polygons into a representative line.

    Right now, the algorithm works like this:
    1. Create a union of all input roi contours by adding a buffer to the exteriors of each
    2. Compute centerline using centerline library
    3. Group points into cluster based on max distance between points
    4. Interpolate line to smooth it

    Args:
        contours (t.List[Contour]): ROI contours
        dcm_info (DCMInfo): DICOM info
        interpolation_distance (int, optional): Interpolation distance to pass into
            centerline algorithm. Defaults to 2.
        max_gap_distance (int, optional): Maximum distance between two points for
            them to be considered in same contour. Defaults to 20 pixel.

    Returns:
        t.Tuple[t.List[dict], t.Tuple[float, float, float, float]]: _description_
    """

    # Create a polygon with a spatial buffer for each contour
    polys = [Polygon(contour.points).buffer(1) for contour in contours]
    # Get the union of all polygons
    union = unary_union(polys)
    # Calculate the centerline of the now single polygon using a voronoi algorithm
    # provided by the centerline library
    geom = Centerline(union, interpolation_distance=interpolation_distance)
    lines = np.array([line.xy for line in geom.geoms])
    # Scale the points
    lines[:, 0, :] /= dcm_info.pixel_spacing[0]
    lines[:, 1, :] /= dcm_info.pixel_spacing[1]
    # restack and interpolate (linear, we have duplicate x ~ take the average y)
    lines_c = np.concatenate((lines[:, :, 0], lines[:, :, 1]))
    # split in cluster in case there are gaps in the centerline
    db = DBSCAN(eps=max_gap_distance, min_samples=10).fit(lines_c)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_
    n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
    points = []
    for cluster in range(n_clusters):
        cluster_lines = lines_c[labels == cluster, :]
        interpol_func = interpolate.interp1d(cluster_lines[:, 0], cluster_lines[:, 1], assume_sorted=False, bounds_error=False)
        # resample
        min_x, max_x = cluster_lines[:,0].min(), cluster_lines[:,0].max()
        min_y, max_y = cluster_lines[:,1].min(), cluster_lines[:,1].max()
        pixel_step = 2
        x_range = np.arange(min_x, max_x, pixel_step)
        lines_ci = np.stack((x_range, interpol_func(x_range)), axis=1)
        cluster_points = []
        for i in range(len(lines_ci) - 1):
            cluster_points.append(get_point(lines_ci[i,:], lines_ci[i+1,:]))
        points.append(cluster_points)
    # Get bounding box
    left = lines_c[:, 0].min()
    top = lines_c[:, 1].min()
    width = lines_c[:, 0].max() - left
    height = lines_c[:, 1].max() - top
    return points, (left, top, width, height)


def get_rois(
    rt_dcm: DICOM,
    ct_dcm: DICOMCollection
) -> t.Tuple[t.Dict[int, ROI], DCMInfo]:
    """Get ROIs from a given RT dicom and reference CT dicom.

    Args:
        rt_dcm (DICOM): RTStruct DICOM
        ct_dcm (DICOMCollection): Reference CT dicom.

    Returns:
        t.Tuple[t.Dict[int, ROI], DCMInfo]:
            - Dictionary of ROI number to ROI
            - Relevant DCM info from reference DICOM.
    """
    sops_to_frame = sop_to_frame_mapping(ct_dcm)
    all_rois = dict()

    # ref: https://dicom.nema.org/medical/dicom/current/output/html/part03.html
    # A.19.3: required modules:
    #   * Structure Set
    #   * ROI Contour
    #   * RT ROI Observations

    # C.8.8.5 Structure Set Module
    for roi_info in rt_dcm.StructureSetRoiSequence:
        entry = all_rois.setdefault(roi_info.ROINumber, {})
        entry.update({
            'name': roi_info.ROIName,
            'description': roi_info.ROIDescription,
            'num': roi_info.ROINumber
        })

    # C.8.8.6 ROI Contour Module
    for roi_seq in rt_dcm.ROIContourSequence:
        color = roi_seq.ROIDisplayColor
        roi_num = roi_seq.ReferencedROINumber
        contours = []
        for contour_raw in roi_seq.ContourSequence:
            # For now assume only one Referenced Image
            im_ref = contour_raw.ContourImageSequence[0]
            frame_index = sops_to_frame[im_ref.ReferencedSOPInstanceUID]
            num_points = contour_raw.NumberOfContourPoints
            points = np.reshape(contour_raw.ContourData, (num_points, 3))
            x_min, x_max = points[:,0].min(), points[:,0].max()
            y_min, y_max = points[:,1].min(), points[:,1].max()
            contour = Contour(**{
                'points': points,
                'frame_index': frame_index,
                'left': x_min,
                'top': y_min,
                'width': x_max - x_min,
                'height': y_max - y_min,
                'type_': contour_raw.ContourGeometricType,
            })
            contours.append(contour)
        entry = all_rois.get(roi_num)
        if not entry:
            raise RuntimeError(f"Missing ROI info for ROI number {roi_num}")
        entry.update({
            'contours': contours,
            'color': color,
        })

    # C.8.8.8 ROI Observations Module
    for roi_obs in rt_dcm.RTROIObservationsSequence:
        pass
        # NOTE: doesn't seem to be anything useful in here from MIM
        # Can revisit if necessary, e.g.:
        # (3006, 0082) Observation Number                  IS: '1'
        # (3006, 0084) Referenced ROI Number               IS: '1'
        # (3006, 0088) ROI Observation Description         ST:
        #   'Type:Soft,Range:*/*,Fill:0,Opacity:0.0,'
        #   'Thickness:1,LineThickness:2,read-only:true'
        # (3006, 00a4) RT ROI Interpreted Type             CS: ''
        # (3006, 00a6) ROI Interpreter                     PN: ''
        # (3773, 0030) Private Creator                     LO: 'MIM Software Inc.'
        # (3773, 3005) Private tag data                    UN: Array of 82 elements

    rois = dict()
    for num, roi in all_rois.items():
        if len(roi.get('contours', [])) > 0:
            rois[num] = ROI(**roi)

    pixel_spacing = ct_dcm.get('PixelSpacing')
    dcm_info = DCMInfo(
        rows=ct_dcm.get('Rows'),
        columns=ct_dcm.get('Columns'),
        # NOTE: for some reason RTStruct pixel scaling is transposed
        pixel_spacing=(pixel_spacing[1], pixel_spacing[0]),
        slices=len(ct_dcm)
    )
    return rois, dcm_info


def annotation_template(roi: ROI) -> t.Dict:
    """Generate an annotation template from an ROI.

    Args:
        roi (ROI): ROI

    Returns:
        t.Dict: Annotation template
    """
    annotation = {
        'color': f'rgba({roi.color[0]},{roi.color[1]},{roi.color[2]},0.2)',
        'visible': True,
        'handles': {
            'points': [],
            'textBox': {
                'x': 0,
                'y': 0,
                'boundingBox': None
            }
        },
        'polyBoundingBox': {
            'left': None,
            'top': None,
            'width': None,
            'height': None,
        },
        'frameIndex': None,
        'sliceNumber': None,
        'measurementNumber': roi.num * 10_000,
        'lesionNamingNumber': roi.num * 10_000,
        'location': roi.name,
        'toolType': 'FreehandRoi',
        'active': False,
        'invalidated': False,
        'highlight': False,
    }
    return annotation


def roi_to_annotations(
    roi: ROI,
    dcm_info: DCMInfo
) -> t.List[t.Dict]:
    """Generate annotation(s) for a given roi.

    Args:
        roi (ROI): Input ROI
        dcm_info (DCMInfo): DICOM information

    Returns:
        t.List[Dict]: List of annotations.
    """
    annotations = []
    # Get dict of contours per frame
    frame_contours = {}
    for contour in roi.contours:
        frame = frame_contours.setdefault(contour.frame_index, [])
        frame.append(contour)
    for frame, contours in frame_contours.items():
        if roi.name.startswith('B-scan'):
            continue
        # Collapse into centerline
        if roi.name in LAYER_ROIS:
            annotation = annotation_template(roi)
            points, (left, top, width, height) = polys_to_centerline(contours, dcm_info)
            # Already grouped by frame index above
            annotation['frameIndex'] = contours[0].frame_index + 1
            annotation['sliceNumber'] = contours[0].frame_index + 1
            annotation['toolType'] = 'OpenFreehandRoi'
            annotation['polyBoundingBox'] = {
                'left': left,
                'top': top,
                'width': width,
                'height': height,
            }
            annotation['handles']['points'] = points
            annotations.append(annotation)
            continue
        # Get annotation for each contour on a given frame.
        for contour in contours:
            if len(np.unique(contour.points[:,2])) > 1:
                raise RuntimeError("ROI not planar")
            annotation = annotation_template(roi)
            points = contour.points[:,0:2]
            # Adjust points with pixel scaling
            points[:, 0] /= dcm_info.pixel_spacing[0]
            points[:, 1] /= dcm_info.pixel_spacing[1]
            # Get bounding box
            left = points[:,0].min()
            top = points[:,1].min()
            annotation['polyBoundingBox'] = {
                'left': left,
                'top': top,
                'width': (points[:,0].max() - left),
                'height': (points[:,1].max() - top),
            }
            # Set frame index
            # NOTE: FrameIndex increments for each plane in multiframe,
            # as well as sliceNumber. For non-multiframe images, frameIndex is
            # 0 for all planes, but sliceNumber increments
            annotation['frameIndex'] = contour.frame_index + 1
            annotation['sliceNumber'] = contour.frame_index + 1
            # Use shapely to ensure close polygon and generate points.
            poly = Polygon(points)
            points_raw = poly.exterior.xy
            points = np.zeros((len(points_raw[0]),2))
            points[:,0] = points_raw[0]
            points[:, 1] = points_raw[1]
            for i, point in enumerate(points):
                p1 = point
                if i + 1 < points.shape[0]:
                    p2 = points[i + 1, 0:2]
                else:
                    p2 = points[0, 0:2]
                annotation['handles']['points'].append(
                    get_point(p1, p2)
                )
            annotations.append(annotation)
    return annotations


def create_annotation(
    fw: FWClient,
    task_id: str,
    file_id: str,
    roi: ROI,
    opt_dcm: DICOM,
    dcm_info: DCMInfo,
) -> None:
    """Add an annotation to the specified task for the given ROI.

    Args:
        fw (FWClient): Flywheel client.
        task_id (str): Flywheel task id.
        file_id (str): Flywheel file id.
        roi (ROI): ROI generated from `get_rois`
        opt_dcm (DICOM): OPT reference dicom
        dcm_info (DCMInfo): DICOM info.
    """
    user = fw.auth_status
    data = roi_to_annotations(roi, dcm_info)
    study_uid = opt_dcm.StudyInstanceUID
    series_uid = opt_dcm.SeriesInstanceUID
    sop_uid = opt_dcm.SOPInstanceUID

    for i, annotation in enumerate(data):
        path = (
            f"{study_uid}$$${series_uid}$$${sop_uid}"
            f"$$${annotation['frameIndex']}"
        )
        annotation['measurementNumber'] += i
        annotation['lesionNamingNumber'] += i

        annotation.update({
            'SeriesInstanceUID': series_uid,
            'StudyInstanceUID': study_uid,
            'SOPInstanceUID': sop_uid,
            'uuid': str(uuid.uuid4()),
            'path': path,
            'flywheelOrigin': user.origin,
            '_id': str(uuid.uuid4()),
            'imagePath': path
        })

        annotation_info = {
            'file_id': file_id,
            'task_id': task_id,
            'data': annotation
        }

        #return annotation_info
        _ = fw.post('/api/annotations', json=annotation_info)


if __name__ == '__main__':
    # Demo
    fw = FWClient(api_key=os.environ.get('FW_TASKS_KEY'))
    task_id = '62fe700eab89267ea6fb45a7'
    file_id = '62f56c92e1bb2aa4635d6751'
    rt_dcm = DICOM('./resources/rt-struct.dcm')
    opt_dcm = DICOM('./resources/opt-dcm.dcm')
    ct_dcm = DICOMCollection.from_zip('./resources/ct-dcm.dicom.zip')

    # Extract ROIs from dicoms using get_rois function from converter.py
    rois, dcm_info = get_rois(rt_dcm, ct_dcm)

    # Upload all ROIs as annotations
    rois = {r.name: r for r in rois.values()}
    for roi in rois.values():
        print(roi.name)
        create_annotation(fw, task_id, file_id, roi, opt_dcm, dcm_info)
