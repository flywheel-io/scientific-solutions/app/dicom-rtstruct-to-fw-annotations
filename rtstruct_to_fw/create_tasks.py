import sys
import json
import bson
from fw_client import FWClient

def create_annotations(annotations, task_id, api_key, file_id, user_email):
    client = FWClient(api_key=api_key)
    for annotation in annotations:
        del annotation['file_ref']
        del annotation['_id']
        del annotation['created']
        del annotation['modified']
        del annotation['deleted']
        del annotation['revision']

        annotation['file_id'] = file_id
        annotation['task_id'] = task_id
        origin = {'type': 'user', 'id': user_email}
        annotation['origin'] = origin
        annotation['data']['flywheelOrigin'] = origin
        annotation['data']['_id'] = str(bson.ObjectId())
        client.post('/api/annotations', json=annotation)


if __name__ == '__main__':
    try:
        path = sys.argv[1]
        api_key = sys.argv[2]
        task_id = sys.argv[3]
        file_id = sys.argv[4]
        user_email = sys.argv[5]

        with open(path) as fp:
            annotations = json.load(fp)
    except Exception as exc:
        print(exc)
        print(
            "Usage: python create_tasks.py <tasks.json> <api_key> <task_id> "
            "<file_id> <user_email>"
        )
        sys.exit(1)

    if 'results' in annotations:
        annotations = annotations['results']

    create_annotations(annotations, api_key, task_id, file_id, user_email)

